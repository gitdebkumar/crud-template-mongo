FROM openjdk:18-ea-11-jdk-alpine3.15
ARG JAR_FILE=target/*.jar
WORKDIR /app
VOLUME ["/tmp","/app/logs"]
COPY ${JAR_FILE} application.jar
ENTRYPOINT ["java","-jar","application.jar"]