package org.bb.crud.api;

import java.util.Optional;

import org.bb.crud.model.mongo.MasterData;
import org.bb.crud.service.MasterDataService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest({ MasterDataRestController.class })
public class MasterDataRestControllerTest {

	@Autowired
	MockMvc mockMvc;

	@Autowired
	ObjectMapper mapper;

	@MockBean
	MasterDataService masterDataService;
	
//	@MockBean
//	MasterDataRepository masterDataRepository;

	MasterData RECORD_1 = new MasterData("id1", "Ram", "Ram Kumar", "{}");
	MasterData RECORD_2 = new MasterData("id2", "Sam", "Sam Sundar", "{}");
	MasterData RECORD_3 = new MasterData("id3", "Jadu", "Jadu Halder", "{}");

	@Test
	public void getMasterData_success() throws Exception {
		Mockito.when(masterDataService.getMasterData(RECORD_2.getMasterDataId())).thenReturn(Optional.of(RECORD_2));

		mockMvc.perform(MockMvcRequestBuilders.get("/master/division/Sam"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.masterDataId", Matchers.is("Sam")));
	}

//	@Test
//	public void saveMasterData_success() throws Exception {
//		MasterData record = MasterData.builder()
//				.id("id4")
//				.masterDataId("Madhu")
//				.masterDataName("Madhu Samui")
//				.masterData("{}")
//				.build();
//
//		Mockito.when(masterDataService.saveMasterData(record)).thenReturn(record);
//
//		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders
//				.post("/master/set")
//				.contentType(MediaType.APPLICATION_JSON)
//				.accept(MediaType.APPLICATION_JSON)
//				.content(this.mapper.writeValueAsString(record));
//
//		mockMvc.perform(mockRequest)
//			.andExpect(MockMvcResultMatchers.status().isOk())
//			.andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.notNullValue()))
//			.andExpect(MockMvcResultMatchers.jsonPath("$.masterDataId", Matchers.is("Madhu")));
//	}	
}
