package org.bb.crud.repository.mongo.impl;

import java.lang.reflect.Array;
import java.util.LinkedList;
import java.util.List;

import org.bb.crud.model.dto.MongoQueryRequestDto;
import org.bb.crud.repository.mongo.CustomizedAggregationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.Fields;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.aggregation.SortOperation;
import org.springframework.data.mongodb.core.query.Criteria;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class CustomizedAggregationRepositoryImpl<T> implements CustomizedAggregationRepository<T> {

	private final MongoTemplate mongoTemplate;

	@Autowired
	public CustomizedAggregationRepositoryImpl(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

	@Override
	public Page<T> getAnswerForAggregation(MongoQueryRequestDto aggregationRequest, PageRequest page, Class<T> outputType) {
		List<AggregationOperation> aggregationList = new LinkedList<AggregationOperation>();
		GroupOperation groupOperation = null;
		ProjectionOperation projectOperation = Aggregation.project();
		MatchOperation filterOperation = null, havingOperation = null;
		SortOperation sortOperation = null;

		if (!(aggregationRequest.getCondition() == null || aggregationRequest.getCondition().isEmpty())) {
			List<Criteria> filterCriteriaList = new LinkedList<Criteria>();
			DBObject filterObject = BasicDBObject.parse(aggregationRequest.getCondition());
			for (String key : filterObject.keySet()) {
				Criteria c = new Criteria(key);
				Object conditionObject = filterObject.get(key);
				if (conditionObject instanceof DBObject) {
					DBObject filterCondition = (DBObject) conditionObject;
					appendCondition(c, filterCondition);					
				} else
					c.is(conditionObject);
				filterCriteriaList.add(c);
			}
			filterOperation = Aggregation.match(new Criteria().andOperator(filterCriteriaList.toArray(new Criteria[0])));
		}

		if (!(aggregationRequest.getSort() == null || aggregationRequest.getSort().isEmpty())) {
			DBObject sortObject = BasicDBObject.parse(aggregationRequest.getSort());
			for (String key : sortObject.keySet()) {
				Sort s = null;
				if ("1".equals(sortObject.get(key).toString()))
					s = Sort.by(Direction.ASC, key + "_value");
				else if ("-1".equals(sortObject.get(key).toString()))
					s = Sort.by(Direction.DESC, key + "_value");

				if (s == null)
					continue;

				sortOperation = sortOperation == null ? Aggregation.sort(s) : sortOperation.and(s);
			}
		}
		boolean defaultSort = sortOperation == null;

		if (!(aggregationRequest.getAggregationGroup() == null || aggregationRequest.getAggregationGroup().isEmpty())) {
			if (defaultSort) {
				Sort s = Sort.by(Direction.ASC, "_id");
				sortOperation = Aggregation.sort(s);
			}

			int index = 1;
			Fields fields = Fields.fields();
			DBObject groupByObject = BasicDBObject.parse(aggregationRequest.getAggregationGroup());
			for (String key : groupByObject.keySet()) {
				if ("1".equals(groupByObject.get(key).toString())) {
					fields = fields.and("grpKey" + index, key);
					projectOperation = projectOperation.andExpression("_id." + "grpKey" + index).as("grpKey" + index);
					if (defaultSort) {
						Sort s = Sort.by(Direction.ASC, "_id." + "grpKey" + index);
						sortOperation = sortOperation.and(s);
					}
					index++;
				}
			}
			groupOperation = Aggregation.group(fields);

			if (!(aggregationRequest.getAggregationValue() == null || aggregationRequest.getAggregationValue().isEmpty())) {
				index = 1;
				DBObject groupValueObject = BasicDBObject.parse(aggregationRequest.getAggregationValue());
				for (String key : groupValueObject.keySet()) {
					switch (key.toLowerCase()) {
					case "count":
						groupOperation = groupOperation.count().as("count_value");
						projectOperation = projectOperation.andExpression("count_value").as("aggVal" + index++);
						break;
					case "avg":
						groupOperation = groupOperation.avg(groupValueObject.get(key).toString()).as("avg_value");
						projectOperation = projectOperation.andExpression("avg_value").as("aggVal" + index++);
						break;
					case "sum":
						groupOperation = groupOperation.sum(groupValueObject.get(key).toString()).as("sum_value");
						projectOperation = projectOperation.andExpression("sum_value").as("aggVal" + index++);
						break;
					case "min":
						groupOperation = groupOperation.min(groupValueObject.get(key).toString()).as("min_value");
						projectOperation = projectOperation.andExpression("min_value").as("aggVal" + index++);
						break;
					case "max":
						groupOperation = groupOperation.max(groupValueObject.get(key).toString()).as("max_value");
						projectOperation = projectOperation.andExpression("max_value").as("aggVal" + index++);
						break;
					}
				}
			}
		}

		if (!(aggregationRequest.getAggregationGroupHaving() == null || aggregationRequest.getAggregationGroupHaving().isEmpty())) {
			List<Criteria> havingCriteriaList = new LinkedList<Criteria>();
			DBObject groupHavingObject = BasicDBObject.parse(aggregationRequest.getAggregationGroupHaving());
			for (String key : groupHavingObject.keySet()) {
				Criteria c = null;
				switch (key.toLowerCase()) {
				case "count":
					c = new Criteria("count_value");
					break;
				case "avg":
					c = new Criteria("avg_value");
					break;
				case "sum":
					c = new Criteria("sum_value");
					break;
				case "min":
					c = new Criteria("min_value");
					break;
				case "max":
					c = new Criteria("max_value");
					break;
				default:
					c = new Criteria(key);
				}

				Object conditionObject = groupHavingObject.get(key);
				if (conditionObject instanceof DBObject) {
					DBObject groupHavingCondition = (DBObject) conditionObject;
					appendCondition(c, groupHavingCondition);					
				} else {
					c.is(conditionObject);
				}
				havingCriteriaList.add(c);
			}
			havingOperation = Aggregation.match(new Criteria().andOperator(havingCriteriaList.toArray(new Criteria[0])));
		}
		
		if (filterOperation != null)
			aggregationList.add(filterOperation);
		if (groupOperation != null)
			aggregationList.add(groupOperation);
		if (havingOperation != null)
			aggregationList.add(havingOperation);
		if (sortOperation != null)
			aggregationList.add(sortOperation);
		if (projectOperation != null)
			aggregationList.add(projectOperation);

		// SkipOperation skipOperation = Aggregation.skip((long) page.getPageNumber() * page.getPageSize());
		// LimitOperation limitOperation = Aggregation.limit(page.getPageSize());
		// Aggregation aggregation = Aggregation.newAggregation(filterOperation, groupOperation, matchOperation, sortOperation, skipOperation, limitOperation, projectOperation);
		// Aggregation aggregation = Aggregation.newAggregation(filterOperation, groupOperation, havingOperation, sortOperation, projectOperation);
		Aggregation aggregation = Aggregation.newAggregation(aggregationList);
		AggregationResults<T> result = mongoTemplate.aggregate(aggregation, "answer", outputType);

		List<T> mappedResult = result.getMappedResults();
		int total = mappedResult.size();
		if (total <= 0)
			return Page.empty(page);

		int startIndex = Math.min(total, page.getPageNumber() * page.getPageSize()), endIndex = Math.min(total, (1 + page.getPageNumber()) * page.getPageSize());
		List<T> subResult = mappedResult.subList(startIndex, endIndex);

		return new PageImpl<T>(subResult, page, mappedResult.size());
	}

	private void appendCondition(Criteria c, DBObject condition) {
		for (String operator : condition.keySet()) {
			switch (operator.toLowerCase()) {
			case "eq":
				c.is(condition.get(operator));
				break;
			case "ne":
				c.ne(condition.get(operator));
				break;
			case "gt":
				c.gt(condition.get(operator));
				break;
			case "gte":
				c.gte(condition.get(operator));
				break;
			case "lt":
				c.lt(condition.get(operator));
				break;
			case "lte":
				c.lte(condition.get(operator));
				break;
			case "in":
				c.in(condition.get(operator));
				break;
			case "nin":
				c.nin(condition.get(operator));
				break;
			}
		}
	}
}
