package org.bb.crud.repository.mongo;

import org.bb.crud.model.dto.MongoQueryRequestDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface CustomizedAggregationRepository<T> {
	
	Page<T> getAnswerForAggregation(MongoQueryRequestDto aggregationRequest, PageRequest page, Class<T> outputType);
}