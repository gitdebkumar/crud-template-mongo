package org.bb.crud.repository.mongo;

import java.util.List;

import org.bb.crud.model.mongo.MasterData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MasterDataRepository extends MongoRepository<MasterData, String> {	// , QuerydslPredicateExecutor<MasterData>

    @Query("{ 'masterDataId' : ?0 }")
    List<MasterData> findMasterDataById(@Param("masterDataId") String masterDataId);
}