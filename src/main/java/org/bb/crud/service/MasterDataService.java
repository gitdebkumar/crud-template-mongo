package org.bb.crud.service;

import java.util.List;
import java.util.Optional;

import org.bb.crud.model.mongo.MasterData;
import org.bb.crud.repository.mongo.MasterDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MasterDataService {

	@Autowired
	private MasterDataRepository masterDataRepository;
	
	public Optional<MasterData> getMasterData(String masterDataId) {
		List<MasterData> matchingSets = masterDataRepository.findMasterDataById(masterDataId);
		return matchingSets.stream().findFirst();
	}

	public MasterData saveMasterData(MasterData masterData) {
		return masterDataRepository.save(masterData);
	}
}
