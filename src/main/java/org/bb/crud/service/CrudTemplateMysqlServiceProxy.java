package org.bb.crud.service;

import java.util.List;

import org.bb.crud.model.dto.Station;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("crud-template-mysql")
@RibbonClient("crud-template-mysql")
public interface CrudTemplateMysqlServiceProxy {

	@GetMapping(value = "/master/station", consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<Station> getAllStation();
	
	@GetMapping(value = "/master/test")
	public String testing();
}
