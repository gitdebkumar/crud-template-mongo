package org.bb.crud.api;

import java.util.List;

import org.bb.crud.model.dto.Station;
import org.bb.crud.service.CrudTemplateMysqlServiceProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/intra")
public class IntraServiceRestController {

	Logger log = LoggerFactory.getLogger(IntraServiceRestController.class);

	@Autowired
	private CrudTemplateMysqlServiceProxy proxy;
	
	@GetMapping(value = "/station", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Station> getAllStation() {
		return proxy.getAllStation();
	}

	@GetMapping(value = "/test")
	public String testing() {
		return proxy.testing();
	}
}
