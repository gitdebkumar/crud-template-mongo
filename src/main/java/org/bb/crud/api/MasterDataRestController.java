package org.bb.crud.api;

import java.util.Optional;

import org.bb.crud.model.common.ResponseDto;
import org.bb.crud.model.mongo.MasterData;
import org.bb.crud.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RestController
@RequestMapping("/master")
public class MasterDataRestController {

	Logger log = LoggerFactory.getLogger(MasterDataRestController.class);

	private final ObjectMapper mapper = new ObjectMapper();

	/** The master data service
	 */
	@Autowired
	private MasterDataService masterDataService;

	/** Returns the list of divisions based on the given master data id
	 * @param masterDataId The master data id parameter representing group id
	 * @return JSON object that contains all sub divisions.
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 */
	@GetMapping(value = "/division/{masterDataId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public String getMasterDataById(@PathVariable String masterDataId) throws JsonMappingException, JsonProcessingException {
		Optional<MasterData> masterDataOpt = masterDataService.getMasterData(masterDataId);
		MasterData masterData = masterDataOpt.get();
		ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put("masterDataId", masterData.getMasterDataId());
		rootNode.put("masterDataName", masterData.getMasterDataName());
		rootNode.put("masterData", mapper.readTree(masterData.getMasterData()));
		return rootNode.toString();
	}

	/** Save master data in form of JSON object
	 * @param masterDataStr JSON Object
	 * @return Returns the success response
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 */
	@PostMapping(value = "/division/set")
	public ResponseDto saveMasterData(@RequestBody String masterDataStr) throws JsonMappingException, JsonProcessingException {
		JsonNode masterDataObj = mapper.readTree(masterDataStr);
		String masterDataId = masterDataObj.get("masterDataId").asText();
		String masterDataName = masterDataObj.get("masterDataName").asText();
		String masterData = masterDataObj.get("masterData").toString();
		masterDataService.saveMasterData(new MasterData(null, masterDataId, masterDataName, masterData));
		return new ResponseDto("success");
	}
}
