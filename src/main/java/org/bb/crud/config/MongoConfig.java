package org.bb.crud.config;

import java.util.Arrays;

import org.bb.crud.model.mongo.converter.DocumentToMasterDataConverter;
import org.bb.crud.model.mongo.converter.MasterDataToDocumentConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;

@Configuration
public class MongoConfig {

	@Bean
    public MongoCustomConversions mongoCustomConversions(){
	    return new MongoCustomConversions(Arrays.asList(
	        new DocumentToMasterDataConverter(),
	        new MasterDataToDocumentConverter()
	        )
	    );
    }
}
