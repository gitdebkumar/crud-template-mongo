package org.bb.crud.model.mongo.converter;

import org.bb.crud.model.mongo.MasterData;
import org.bson.Document;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.stereotype.Component;

@Component
@ReadingConverter
public class DocumentToMasterDataConverter implements Converter<Document, MasterData> {

	@Override
	public MasterData convert(Document masterDataDoc) {
		String id = masterDataDoc.getObjectId("_id").toString();
		String masterDataId = masterDataDoc.getString("masterDataId");
		String masterDataName = masterDataDoc.getString("masterDataName");
		String masterData = ((Document) masterDataDoc.get("masterData")).toJson();

		MasterData md = new MasterData(id, masterDataId, masterDataName, masterData);
		return md;
	}
}
