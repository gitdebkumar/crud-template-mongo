package org.bb.crud.model.mongo.converter;

import org.bb.crud.model.mongo.MasterData;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.stereotype.Component;

import com.mongodb.BasicDBObject;

@Component
@WritingConverter
public class MasterDataToDocumentConverter implements Converter<MasterData, Document> {

	@Override
	public Document convert(MasterData masterData) {
		Document doc = new Document();
		if (masterData.getId() != null)
			doc.put("_id", new ObjectId(masterData.getId()));
		doc.put("masterDataId", masterData.getMasterDataId());
		doc.put("masterDataName", masterData.getMasterDataName());
		doc.put("masterData", BasicDBObject.parse(masterData.getMasterData()));
		doc.remove("_class");
		return doc;
	}
}
