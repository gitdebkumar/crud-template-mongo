package org.bb.crud.model.mongo;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

//@QueryEntity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document("masterdata")
public class MasterData implements Serializable {

	private static final long serialVersionUID = 7686437589318756552L;

	@Id
    private String id;
    
    @Indexed
    @NonNull
    private String masterDataId;
    
    @NonNull
    private String masterDataName;
    
    private String masterData;
}
