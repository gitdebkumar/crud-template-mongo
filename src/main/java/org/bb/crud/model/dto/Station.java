package org.bb.crud.model.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class Station implements Serializable {

	private static final long serialVersionUID = 7686437589318756552L;

    private Integer id;

    private String stationCode;
    
    private String stationName;
}
