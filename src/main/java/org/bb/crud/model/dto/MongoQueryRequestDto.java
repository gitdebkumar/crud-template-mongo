package org.bb.crud.model.dto;

import org.bb.crud.model.common.RequestDto;

public class MongoQueryRequestDto extends RequestDto {

	private String projection;
	private String condition;
	private String aggregationGroup;
	private String aggregationGroupHaving;
	private String aggregationValue;
	private String sort;
	private int pageNo;
	private int pageSize;

	public String getProjection() {
		return projection;
	}

	public void setProjection(String projection) {
		this.projection = projection;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getAggregationGroup() {
		return aggregationGroup;
	}

	public void setAggregationGroup(String aggregationGroup) {
		this.aggregationGroup = aggregationGroup;
	}

	public String getAggregationGroupHaving() {
		return aggregationGroupHaving;
	}

	public void setAggregationGroupHaving(String aggregationGroupHaving) {
		this.aggregationGroupHaving = aggregationGroupHaving;
	}

	public String getAggregationValue() {
		return aggregationValue;
	}

	public void setAggregationValue(String aggregationValue) {
		this.aggregationValue = aggregationValue;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public MongoQueryRequestDto() {
		super();
	}
}
