package org.bb.crud.model.common;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiResponseDto<T> extends ResponseDto {
	
	private T data;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public ApiResponseDto() {
		super();
	}

	public ApiResponseDto(String status, String message, T data) {
		super(status, message);
		this.data = data;
	}
}
