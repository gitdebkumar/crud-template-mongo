package org.bb.crud.model.common;

public class SortField {

	private String fieldName;
	private SortType sortType;

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public SortType getSortType() {
		return sortType;
	}

	public void setSortType(SortType sortType) {
		this.sortType = sortType;
	}

	public enum SortType {
		ASC, DESC
	}
}
