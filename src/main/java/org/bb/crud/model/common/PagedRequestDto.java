package org.bb.crud.model.common;

import java.util.List;

public class PagedRequestDto extends RequestDto {

	private int pageNo;
	private int pageSize;
	private List<SearchField> searchBy;
	private List<SortField> sortBy;

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public List<SearchField> getSearchBy() {
		return searchBy;
	}

	public void setSearchBy(List<SearchField> searchBy) {
		this.searchBy = searchBy;
	}

	public List<SortField> getSortBy() {
		return sortBy;
	}

	public void setSortBy(List<SortField> sortBy) {
		this.sortBy = sortBy;
	}
	
	public PagedRequestDto() {
	}

	public PagedRequestDto(int pageNo, int pageSize) {
		this.pageNo = pageNo;
		this.pageSize = pageSize;
	}
}
